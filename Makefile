#
# Makefile for the Debian Science tools
#
# Copyright (C) 2008  Manuel Prinz <manuel@debian.org>
#
# You may freely redistribute, use and modify this software under the terms of
# the GNU General Public License, either version 3 of the License, or (at your
# option) any later version.
#

TOOLS="wnpp"

all:

install:
	install -d $(DESTDIR)/usr/bin
	install src/debsci $(DESTDIR)/usr/bin
	for t in $(TOOLS); do \
		install src/debsci-$$t $(DESTDIR)/usr/bin ; \
	done

.PHONY: all install
